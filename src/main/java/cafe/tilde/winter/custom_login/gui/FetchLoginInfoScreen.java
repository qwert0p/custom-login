package cafe.tilde.winter.custom_login.gui;

import cafe.tilde.winter.custom_login.util.LoginServerMotdSequence;
import cafe.tilde.winter.custom_login.util.YggdrasilMetadata;
import com.google.gson.Gson;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class FetchLoginInfoScreen extends Screen {
    private final Screen parent;
    private final LoginServerMotdSequence motdSequence;
    private final ServerInfo entry;
    private Text displayedTitle;
    private boolean hasAskForFileWidgets;

    public FetchLoginInfoScreen(Screen parent, LoginServerMotdSequence motdSequence, ServerInfo entry) {
        super(Text.translatable("gui.custom_login.fetching.title"));
        this.parent = parent;
        this.motdSequence = motdSequence;
        this.entry = entry;
        displayedTitle = title;
        hasAskForFileWidgets = false;
    }

    @Override
    public void init() {
        super.init();
        assert client != null;
        // find the correct URL for the yggdrasil metadata file
        URL metaFileURL = motdSequence.getMetaFileURL(entry.address);
        if (metaFileURL == null) {
            // we have to ask the user for either the location or the actual endpoints
            displayedTitle = Text.translatable("gui.custom_login.fetching.ask.title");
            if (motdSequence.getMetaFileLocation() == LoginServerMotdSequence.MetaFileLocation.ASK_FOR_FILE) {
                addAskForFileWidgets();
            } else {  // it must be ASK_FOR_ENDPOINTS
                // addAskForEndpointsWidgets - NYI
                // instead display an error toast and leave
                client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                         Text.translatable("gui.custom_login.error.title"),
                         Text.translatable("gui.custom_login.nyi.description")));
                client.setScreen(parent);
            }
            return;
        }
        fetchAndContinue(metaFileURL);
    }

    private void fetchAndContinue(URL metaFileURL) {
        assert client != null;
        // actually get the file and store it in a new YggdrasilMetadata
        Gson gson = new Gson();
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(metaFileURL.toURI())
                    .header("Accept", "application/json")
                    .GET()
                    .build();
            HttpClient.newHttpClient().sendAsync(request, HttpResponse.BodyHandlers.ofString()).thenAccept(response -> {
                String jsonString = response.body();
                YggdrasilMetadata metadata = gson.fromJson(jsonString, YggdrasilMetadata.class);
                // open the login screen with the metadata
                client.execute(() -> client.setScreen(new LoginScreen(parent, metadata, entry)));
            }).exceptionally(error -> {
                // oops, if there are ask widgets it's the users fault, otherwise just leave
                client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                         Text.translatable("gui.custom_login.error.title"),
                         Text.literal(error.getLocalizedMessage())));
                if (!hasAskForFileWidgets) {
                    client.setScreen(parent);
                }
                return null;
            });
        } catch (URISyntaxException ex) {
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                     Text.translatable("gui.custom_login.error.title"),
                     Text.literal(ex.getLocalizedMessage())));
            if (!hasAskForFileWidgets) {
                client.keyboard.setRepeatEvents(false);
                client.setScreen(parent);
            }
        }
    }

    private void addAskForFileWidgets() {
        if (!hasAskForFileWidgets) {
            hasAskForFileWidgets = true;
            assert client != null;
            client.keyboard.setRepeatEvents(true);
            // text field
            TextFieldWidget fileLocationField = addDrawableChild(new TextFieldWidget(textRenderer, width / 2 - 100,
                    height / 2 - 21, 200, 20,  Text.translatable("gui.custom_login.fetching.ask.meta_file")));
            fileLocationField.setMaxLength(256);
            // submit button
            addDrawableChild(new ButtonWidget(width / 2 - 100, height / 2 + 1, 100, 20, Text.translatable("gui.ok"),
                    button -> {
                try {
                    URL metaFileURL = new URL(fileLocationField.getText());
                    fetchAndContinue(metaFileURL);
                } catch (MalformedURLException e) {
                    client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                             Text.translatable("gui.custom_login.error.title"),
                             Text.literal(e.getLocalizedMessage())));
                }
            }));
            // cancel button
            addDrawableChild(new ButtonWidget(width / 2, height / 2 + 1, 100, 20,  Text.translatable("gui.cancel"),
                    button -> {
                client.keyboard.setRepeatEvents(false);
                client.setScreen(parent);
            }));
        }
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        drawCenteredText(matrices, textRenderer, displayedTitle, width / 2, 20, 0xFFFFFF);  // title
        if (hasAskForFileWidgets) {
            // note to player
            drawCenteredText(matrices, textRenderer, Text.translatable("gui.custom_login.fetching.ask.description"),
                    width / 2, 40, 0xAAAAAA);
            // text field label
            drawTextWithShadow(matrices, textRenderer, Text.translatable("gui.custom_login.fetching.ask.meta_file"),
                    width / 2 - 100, height / 2 - 30, 0xFFFFFF);
        }
        super.render(matrices, mouseX, mouseY, delta);
    }
}
