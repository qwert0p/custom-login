package cafe.tilde.winter.custom_login.gui;

import cafe.tilde.winter.custom_login.util.CustomLoginEnvironment;
import cafe.tilde.winter.custom_login.util.LoginHelper;
import cafe.tilde.winter.custom_login.util.YggdrasilMetadata;
import cafe.tilde.winter.custom_login.mixin.MinecraftClientAccessor;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class LoginScreen extends Screen {
    private final Screen parent;
    private final YggdrasilMetadata yggdrasilMetadata;
    private final ServerInfo entry;
    private CustomLoginEnvironment environment;
    private TextFieldWidget usernameField;
    private PasswordFieldWidget passwordField;
    private ButtonWidget logInButton;

    protected LoginScreen(Screen parent, YggdrasilMetadata yggdrasilMetadata, ServerInfo entry) {
        super(Text.translatable("gui.custom_login.login.title", yggdrasilMetadata.name));
        this.parent = parent;
        this.yggdrasilMetadata = yggdrasilMetadata;
        this.entry = entry;
    }

    @Override
    public void init() {
        super.init();
        assert client != null;
        client.keyboard.setRepeatEvents(true);
        environment = CustomLoginEnvironment.create(yggdrasilMetadata);
        // fields
        usernameField = addDrawableChild(new TextFieldWidget(textRenderer, width / 2 - 100, height / 2 - 20, 200, 20,
                Text.translatable("gui.custom_login.username")));
        passwordField = addDrawableChild(new PasswordFieldWidget(textRenderer, width / 2 - 100, height / 2 + 20, 200, 20,
                 Text.translatable("gui.custom_login.password")));
        // log in button
        logInButton = addDrawableChild(new ButtonWidget(width / 2 - 200, height - 40, 200, 20,
                 Text.translatable("gui.custom_login.logIn"), button -> logInAndContinue(
                        usernameField.getText(), passwordField.getUnobfuscatedText())));
        // back button
        addDrawableChild(new ButtonWidget(width / 2, height - 40, 200, 20,
                 Text.translatable("gui.cancel"), button -> {
            // return to previous screen (probably server select screen)
            assert client != null;
            client.keyboard.setRepeatEvents(false);
            client.setScreen(parent);
        }));
    }

    private void logInAndContinue(String username, String password) {
        assert client != null;
        // temporarily disable the "log in" button
        logInButton.active = false;
        Executor executor = Executors.newSingleThreadExecutor();
        LoginHelper.logIn(username, password, environment, entry, executor).thenAccept(sessionWithService -> {
            ((MinecraftClientAccessor) client).setSession(sessionWithService.getSession());
            ((MinecraftClientAccessor) client).setSessionService(sessionWithService.getService());
            // pop a success toast and join the server
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                     Text.translatable("gui.custom_login.success.title"),
                     Text.translatable("gui.custom_login.success.description")));
            client.keyboard.setRepeatEvents(false);
            client.execute(() -> ConnectScreen.connect(parent, client, ServerAddress.parse(entry.address), entry));
        }).exceptionally(e -> {
            // pop a toast on screen to notify the user
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                     Text.translatable("gui.custom_login.login.failed"), Text.literal(e.getCause().getMessage())));
            logInButton.active = true;
            return null;
        });
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        // title
        drawCenteredText(matrices, textRenderer, title, width / 2, 20, 0xFFFFFF);
        // message
        drawCenteredText(matrices, textRenderer, yggdrasilMetadata.message, width / 2, 40, 0xAAAAAA);
        // field labels
        drawTextWithShadow(matrices, textRenderer,  Text.translatable("gui.custom_login.email"), width / 2 - 100,
                height / 2 - 30, 0xFFFFFF);
        drawTextWithShadow(matrices, textRenderer, Text.translatable("gui.custom_login.password"), width / 2 - 100,
                height / 2 + 10, 0xFFFFFF);
        super.render(matrices, mouseX, mouseY, delta);
    }
}
