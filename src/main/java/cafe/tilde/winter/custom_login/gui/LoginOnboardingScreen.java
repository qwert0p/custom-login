package cafe.tilde.winter.custom_login.gui;

import cafe.tilde.winter.custom_login.util.LoginServerMotdSequence;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

public class LoginOnboardingScreen extends Screen {
    private final Screen parent;
    private final LoginServerMotdSequence motdSequence;
    private final ServerInfo entry;

    public LoginOnboardingScreen(Screen parent, LoginServerMotdSequence motdSequence, ServerInfo entry) {
        super(Text.translatable("gui.custom_login.onboarding.title"));
        this.parent = parent;
        this.motdSequence = motdSequence;
        this.entry = entry;
    }

    @Override
    public void init() {
        super.init();
        // yes button (i.e. proceed to login screen)
        addDrawableChild(new ButtonWidget(width / 2 - 201, height - 40, 200, 20,
                 Text.translatable("gui.custom_login.logIn"), button -> {
            assert client != null;
            client.setScreen(new FetchLoginInfoScreen(parent, motdSequence, entry));
        }));
        // no button (i.e. connect normally OR return to previous screen if login is required)
        Text noButtonText =  Text.translatable(
                motdSequence.isLoginRequired() ? "gui.back" : "gui.custom_login.onboarding.continue");
        addDrawableChild(new ButtonWidget(width / 2 + 1, height - 40, 200, 20, noButtonText, button -> {
            assert client != null;
            if (motdSequence.isLoginRequired()) {
                // go back
                client.setScreen(parent);
            } else {
                // connect as normal
                ConnectScreen.connect(parent, MinecraftClient.getInstance(), ServerAddress.parse(entry.address), entry);
            }
        }));
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        // title
        drawCenteredText(matrices, textRenderer, title, width / 2, 20, 0xFFFFFF);
        // more descriptive message
        Text messageText = Text.translatable(motdSequence.isLoginRequired()
                ? "gui.custom_login.onboarding.required"
                : "gui.custom_login.onboarding.optional");
        drawCenteredText(matrices, textRenderer, messageText, width / 2, 50, 0x808080);
        super.render(matrices, mouseX, mouseY, delta);
    }

}
