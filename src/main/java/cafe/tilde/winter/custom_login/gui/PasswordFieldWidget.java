package cafe.tilde.winter.custom_login.gui;

import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.Text;

public class PasswordFieldWidget extends TextFieldWidget {
    private String unobfuscatedText;
    public PasswordFieldWidget(TextRenderer textRenderer, int x, int y, int width, int height, Text text) {
        super(textRenderer, x, y, width, height, text);
        this.unobfuscatedText = "";
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        this.setText(this.unobfuscatedText);
        var didAnything = super.keyPressed(keyCode, scanCode, modifiers);
        if (didAnything) {
            this.unobfuscatedText = this.getText();
        }
        this.setText("*".repeat(this.unobfuscatedText.length()));
        return didAnything;
    }

    @Override
    public boolean charTyped(char chr, int modifiers) {
        this.setText(this.unobfuscatedText);
        var b = super.charTyped(chr, modifiers);
        this.unobfuscatedText = this.getText();
        this.setText("*".repeat(this.unobfuscatedText.length()));
        return b;
    }
    public String getUnobfuscatedText() {
        return this.unobfuscatedText;
    }
}
