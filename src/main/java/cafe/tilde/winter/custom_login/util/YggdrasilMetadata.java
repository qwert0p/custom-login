package cafe.tilde.winter.custom_login.util;

public class YggdrasilMetadata {
    /**
     * The name of the Yggdrasil or Minecraft server which is being logged in to
     */
    public String name;
    /**
     * A short message to be shown to the user before logging in
     */
    public String message;
    /**
     * Auth base API route
     */
    public String auth;
    /**
     * Accounts base API route
     */
    public String account;
    /**
     * Sessions base API route
     */
    public String session;
    /**
     * Services base API route
     */
    public String services;
}
