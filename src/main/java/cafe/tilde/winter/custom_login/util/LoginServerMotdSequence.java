package cafe.tilde.winter.custom_login.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class LoginServerMotdSequence {

    public enum MetaFileLocation {
        HTTP_CURRENT_HOST('0'),
        HTTPS_CURRENT_HOST('1'),
        HTTP_WWW_HOST('2'),
        HTTPS_WWW_HOST('3'),
        HTTP_DEDICATED_HOST('4'),
        HTTPS_DEDICATED_HOST('5'),
        ASK_FOR_FILE('e'),
        ASK_FOR_ENDPOINTS('f');

        private final char code;

        MetaFileLocation(char code) {
            this.code = code;
        }

        public static MetaFileLocation fromCode(char code) {
            for (MetaFileLocation location : values()) {
                if (location.code == code) {
                    return location;
                }
            }
            return null;
        }

    }

    private final MetaFileLocation metaFileLocation;
    private final boolean loginRequired;

    /**
     * Parses the custom login control sequence from the MOTD
     * @param sequence The last 4 characters of the MOTD, i.e. the control sequence
     */
    public LoginServerMotdSequence(String sequence) {
        assert sequence.length() == 4;
        this.metaFileLocation = MetaFileLocation.fromCode(sequence.charAt(1));
        this.loginRequired = sequence.charAt(3) == '1';
    }

    public MetaFileLocation getMetaFileLocation() {
        return metaFileLocation;
    }

    public boolean isLoginRequired() {
        return loginRequired;
    }

    /**
     * @param serverAddress The server address as entered in the Add Server screen
     * @return the URL at which the server's metadata file can be found, or null if the location is unspecified
     */
    public URL getMetaFileURL(String serverAddress) {
        String[] domainParts = serverAddress.split("\\.");
        String secondLevelDomain;
        try {
            secondLevelDomain = String.join(".", Arrays.copyOfRange(domainParts, domainParts.length - 3,
                    domainParts.length - 1));
        } catch (ArrayIndexOutOfBoundsException e) {
            secondLevelDomain = "";
        }
        try {
            return new URL(switch (metaFileLocation) {
                case HTTP_CURRENT_HOST -> String.format("http://%s/.well-known/yggdrasil.json", serverAddress);
                case HTTPS_CURRENT_HOST -> String.format("https://%s/.well-known/yggdrasil.json", serverAddress);
                case HTTP_WWW_HOST -> String.format("http://www.%s/.well-known/yggdrasil.json", secondLevelDomain);
                case HTTPS_WWW_HOST -> String.format("https://www.%s/.well-known/yggdrasil.json", secondLevelDomain);
                case HTTP_DEDICATED_HOST -> String.format("http://ygg.%s/meta.json", secondLevelDomain);
                case HTTPS_DEDICATED_HOST -> String.format("https://ygg.%s/meta.json", secondLevelDomain);
                case ASK_FOR_FILE, ASK_FOR_ENDPOINTS -> "";
            });
        } catch (MalformedURLException ignored) {
            return null;
        }
    }
}
