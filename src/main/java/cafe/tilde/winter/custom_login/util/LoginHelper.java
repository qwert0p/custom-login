package cafe.tilde.winter.custom_login.util;

import com.mojang.authlib.Agent;
import com.mojang.authlib.Environment;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import com.mojang.util.UUIDTypeAdapter;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.util.Session;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;

public final class LoginHelper {
    public static CompletableFuture<SessionWithService> logIn(String username, String password, Environment environment,
                                                                            ServerInfo entry, Executor executor) {
        final MinecraftClient client = MinecraftClient.getInstance();
        return CompletableFuture.supplyAsync(() -> {
            String clientToken = UUID.randomUUID().toString();
            // log in with yggdrasil
            YggdrasilAuthenticationService yas = new YggdrasilAuthenticationService(
                    client.getNetworkProxy(), clientToken, environment
            );
            YggdrasilUserAuthentication yua =
                    (YggdrasilUserAuthentication) yas.createUserAuthentication(Agent.MINECRAFT);
            yua.setUsername(username);
            yua.setPassword(password);
            try {
                yua.logIn();
                // get the info from the authenticator
                String name = yua.getSelectedProfile().getName();
                String uuid = UUIDTypeAdapter.fromUUID(yua.getSelectedProfile().getId());
                String token = yua.getAuthenticatedToken();
                // log out after the info has been gotten
                yua.logOut();
                // build a session and return it
                Session session = new CustomLoginSession(name, uuid, token, Optional.empty(), Optional.empty(),
                        Session.AccountType.MOJANG, client.getSession(), client.getSessionService(), entry.address);
                MinecraftSessionService service = yas.createMinecraftSessionService();
                return new SessionWithService(session, service);
            } catch (AuthenticationException e) {
                throw new CompletionException(e);
            }
        }, executor);
    }
}
