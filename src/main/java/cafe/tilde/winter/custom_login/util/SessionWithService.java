package cafe.tilde.winter.custom_login.util;

import com.mojang.authlib.minecraft.MinecraftSessionService;
import net.minecraft.client.util.Session;

public class SessionWithService {
    private final Session session;
    private final MinecraftSessionService service;

    public SessionWithService(Session session, MinecraftSessionService service) {
        this.session = session;
        this.service = service;
    }

    public Session getSession() {
        return session;
    }

    public MinecraftSessionService getService() {
        return service;
    }
}
