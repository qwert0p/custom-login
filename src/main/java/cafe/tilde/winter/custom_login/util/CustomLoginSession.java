package cafe.tilde.winter.custom_login.util;

import com.mojang.authlib.minecraft.MinecraftSessionService;
import net.minecraft.client.util.Session;

import java.util.Optional;

public class CustomLoginSession extends Session {
    private final Session oldSession;
    private final MinecraftSessionService oldSessionService;
    private final String serverAddress;

    public CustomLoginSession(String username, String uuid, String accessToken, Optional<String> xuid,
                              Optional<String> clientId, AccountType accountType, Session oldSession,
                              MinecraftSessionService oldSessionService, String serverAddress) {
        super(username, uuid, accessToken, xuid, clientId, accountType);
        this.oldSession = oldSession;
        this.oldSessionService = oldSessionService;
        this.serverAddress = serverAddress;
    }

    public Session getOldSession() {
        return oldSession;
    }

    public MinecraftSessionService getOldSessionService() {
        return oldSessionService;
    }

    public String getServerAddress() {
        return serverAddress;
    }
}
