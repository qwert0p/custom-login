package cafe.tilde.winter.custom_login.util;

import com.mojang.authlib.Environment;

public interface CustomLoginEnvironment extends Environment {
    static CustomLoginEnvironment create(YggdrasilMetadata metadata) {
        return new CustomLoginEnvironment() {
            @Override
            public String getAuthHost() {
                return metadata.auth;
            }

            @Override
            public String getAccountsHost() {
                return metadata.account;
            }

            @Override
            public String getSessionHost() {
                return metadata.session;
            }

            @Override
            public String getServicesHost() {
                return metadata.services;
            }

            @Override
            public String getName() {
                return metadata.name;
            }

            @Override
            public String asString() {
                return null;
            }
        };
    }
}
