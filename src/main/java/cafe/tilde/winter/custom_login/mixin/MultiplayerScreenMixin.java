package cafe.tilde.winter.custom_login.mixin;

import cafe.tilde.winter.custom_login.util.CustomLoginSession;
import cafe.tilde.winter.custom_login.util.LoginServerMotdSequence;
import cafe.tilde.winter.custom_login.gui.LoginOnboardingScreen;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(MultiplayerScreen.class)
public abstract class MultiplayerScreenMixin extends Screen {

    protected MultiplayerScreenMixin(Text title) {
        super(title);
    }

    @Redirect(method = "connect()V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/multiplayer/MultiplayerScreen;connect(Lnet/minecraft/client/network/ServerInfo;)V"))
    public void goToLoginScreenIfNeeded(MultiplayerScreen instance, ServerInfo entry) {
        assert client != null;
        // firstly ensure that we are using the original session and session service, or this is the same server
        if (client.getSession().getClass().equals(CustomLoginSession.class)) {
            CustomLoginSession customSession = (CustomLoginSession) client.getSession();

            // if the server address matches that of the session, connect as normal
            if (customSession.getServerAddress().equals(entry.address)) {
                ConnectScreen.connect(this, client, ServerAddress.parse(entry.address), entry);
                // otherwise, if there is a custom login sequence, reset session and open the login flow
            } else if (entry.label.getString().endsWith("§0") || entry.label.getString().endsWith("§1")) {
                ((MinecraftClientAccessor) client).setSessionService(
                        ((CustomLoginSession) client.getSession()).getOldSessionService());
                ((MinecraftClientAccessor) client).setSession(
                        ((CustomLoginSession) client.getSession()).getOldSession()
                );
                String motd = entry.label.getString();
                var motdSeq = new LoginServerMotdSequence(motd.substring(motd.length() - 4));
                if (motdSeq.getMetaFileLocation() == null) {
                    client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                            Text.translatable("gui.custom_login.malformed_motd.title"),
                            Text.translatable("gui.custom_login.malformed_motd.description")));
                    ConnectScreen.connect(this, client, ServerAddress.parse(entry.address), entry);
                    return;
                }
                client.setScreen(new LoginOnboardingScreen(instance, motdSeq, entry));

                // otherwise, connect as normal
            } else {
                // reset the session and session service
                ((MinecraftClientAccessor) client).setSessionService(
                        ((CustomLoginSession) client.getSession()).getOldSessionService());
                ((MinecraftClientAccessor) client).setSession(
                        ((CustomLoginSession) client.getSession()).getOldSession()
                );
                ConnectScreen.connect(this, client, ServerAddress.parse(entry.address), entry);
            }

        } else { // we are using original (Mojang) session service
            if (entry.label.getString().endsWith("§0") || entry.label.getString().endsWith("§1")) {
                // custom login sequence is present
                String motd = entry.label.getString();
                var motdSeq = new LoginServerMotdSequence(motd.substring(motd.length() - 4));
                if (motdSeq.getMetaFileLocation() == null) {
                    client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                            Text.translatable("gui.custom_login.malformed_motd.title"),
                            Text.translatable("gui.custom_login.malformed_motd.description")));
                    ConnectScreen.connect(this, client, ServerAddress.parse(entry.address), entry);
                    return;
                }
                client.setScreen(new LoginOnboardingScreen(instance, motdSeq, entry));
            } else {  // custom login is not present (continue as normal)
                ConnectScreen.connect(this, client, ServerAddress.parse(entry.address), entry);
            }
        }
    }
}
