package cafe.tilde.winter.custom_login.mixin;


import cafe.tilde.winter.custom_login.util.CustomLoginSession;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.world.WorldListWidget;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(WorldListWidget.WorldEntry.class)
public class WorldEntryMixin {
    @Shadow
    @Final
    private MinecraftClient client;

    @Inject(method = "play()V", at = @At("HEAD"))
    public void restoreSession(CallbackInfo ci) {
        assert this.client != null;
        // if custom session is present we restore original one
        if (client.getSession().getClass().equals(CustomLoginSession.class)) {
            // reset the session and session service
            ((MinecraftClientAccessor) client).setSessionService(
                    ((CustomLoginSession) client.getSession()).getOldSessionService());
            ((MinecraftClientAccessor) client).setSession(
                    ((CustomLoginSession) client.getSession()).getOldSession()
            );
        }
    }
}
